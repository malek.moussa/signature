-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 21 juin 2022 à 23:08
-- Version du serveur : 10.4.24-MariaDB
-- Version de PHP : 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `signature`
--

-- --------------------------------------------------------

--
-- Structure de la table `historique`
--

CREATE TABLE `historique` (
  `idsignature` int(11) NOT NULL,
  `idutil` int(11) NOT NULL,
  `id_pdf` int(11) NOT NULL,
  `date_signature` datetime NOT NULL DEFAULT current_timestamp(),
  `signature_valide` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `partage`
--

CREATE TABLE `partage` (
  `id` int(11) NOT NULL,
  `idpdf` int(11) NOT NULL,
  `idstructure` int(11) NOT NULL,
  `idstructure_part` int(11) NOT NULL,
  `date_partage` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `partage`
--

INSERT INTO `partage` (`id`, `idpdf`, `idstructure`, `idstructure_part`, `date_partage`) VALUES
(1, 60, 1, 2, '2022-06-21 20:43:50'),
(2, 60, 2, 2, '2022-06-21 20:43:50'),
(3, 60, 3, 2, '2022-06-21 20:43:50');

-- --------------------------------------------------------

--
-- Structure de la table `structure`
--

CREATE TABLE `structure` (
  `id` int(11) NOT NULL,
  `nom_structure` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `structure`
--

INSERT INTO `structure` (`id`, `nom_structure`) VALUES
(1, 'X'),
(2, 'Y'),
(3, 'Z');

-- --------------------------------------------------------

--
-- Structure de la table `upload`
--

CREATE TABLE `upload` (
  `idd` int(11) NOT NULL,
  `nom_pdf` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  `idutil` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `partage` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `upload`
--

INSERT INTO `upload` (`idd`, `nom_pdf`, `size`, `idutil`, `date`, `partage`) VALUES
(58, '18rapport1.pdf', 507992, 14, '2022-06-18 13:23:46', 1),
(59, '58rapport2.pdf', 813451, 14, '2022-06-18 13:23:58', 1),
(60, '16rapport3.pdf', 539005, 14, '2022-06-18 14:00:38', 1),
(62, '47rapport1.pdf', 507992, 14, '2022-06-21 19:27:09', 0),
(63, '24rapport3.pdf', 539005, 14, '2022-06-21 21:35:17', 0),
(64, '48cv.pdf', 617421, 15, '2022-06-21 21:48:21', 0);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `mot_de_passe` varchar(40) NOT NULL,
  `id` int(11) NOT NULL,
  `compte` int(11) NOT NULL DEFAULT 0,
  `email` varchar(30) NOT NULL,
  `cin` varchar(8) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `idstructure` int(11) NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`mot_de_passe`, `id`, `compte`, `email`, `cin`, `nom`, `prenom`, `idstructure`, `code`) VALUES
('e10adc3949ba59abbe56e057f20f883e', 14, 1, 'malek@gct.com.tn', '12345678', 'Malak', 'Moussa', 1, 5656),
('9982b2a7fceaaee2c8444b5086aee008', 15, 1, 'malek.moussa@ensi-uma.tn\n', '12345678', 'mmmmm', 'mmmmmmmmm', 2, 2568),
('superadmin', 16, 1, 'superadmin', '12345678', 'ben salah', 'ahmed', 1, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `historique`
--
ALTER TABLE `historique`
  ADD PRIMARY KEY (`idsignature`),
  ADD KEY `idutil` (`idutil`),
  ADD KEY `id_pdf` (`id_pdf`);

--
-- Index pour la table `partage`
--
ALTER TABLE `partage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpdf` (`idpdf`),
  ADD KEY `idstructure` (`idstructure`),
  ADD KEY `idstructure_part` (`idstructure_part`);

--
-- Index pour la table `structure`
--
ALTER TABLE `structure`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`idd`),
  ADD KEY `idutil` (`idutil`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idsecteur` (`idstructure`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `historique`
--
ALTER TABLE `historique`
  MODIFY `idsignature` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `partage`
--
ALTER TABLE `partage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `structure`
--
ALTER TABLE `structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `upload`
--
ALTER TABLE `upload`
  MODIFY `idd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `historique`
--
ALTER TABLE `historique`
  ADD CONSTRAINT `historique_ibfk_1` FOREIGN KEY (`idutil`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `historique_ibfk_2` FOREIGN KEY (`id_pdf`) REFERENCES `upload` (`idd`);

--
-- Contraintes pour la table `partage`
--
ALTER TABLE `partage`
  ADD CONSTRAINT `partage_ibfk_1` FOREIGN KEY (`idpdf`) REFERENCES `upload` (`idd`),
  ADD CONSTRAINT `partage_ibfk_2` FOREIGN KEY (`idstructure`) REFERENCES `structure` (`id`),
  ADD CONSTRAINT `partage_ibfk_3` FOREIGN KEY (`idstructure_part`) REFERENCES `structure` (`id`);

--
-- Contraintes pour la table `upload`
--
ALTER TABLE `upload`
  ADD CONSTRAINT `upload_ibfk_1` FOREIGN KEY (`idutil`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`idstructure`) REFERENCES `structure` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
